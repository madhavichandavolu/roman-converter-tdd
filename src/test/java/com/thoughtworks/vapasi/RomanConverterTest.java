package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RomanConverterTest {


    private RomanConverter romanConvertor;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @ParameterizedTest(name = "{index} => Roman Number={0}, Arabic Number={1}")
    @CsvSource({"I, 1", "II, 2", "III, 3", "IV, 4", "V, 5", "VI, 6", "VII, 7",
            "IX, 9", "X, 10", "XXXVI, 36", "MMXII, 2012", "MCMXCVI, 1996"})
    void shouldConvertRomanToArabicNumber(String input, Integer expected) {
        assertEquals(expected, romanConvertor.convertRomanToArabicNumber(input));
    }

    @ParameterizedTest(name = "{index} => input=''{0}''")
    @ValueSource(strings = {"0", "-1"})
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed(String input) {
        assertThrows(IllegalArgumentException.class, () -> romanConvertor.convertRomanToArabicNumber(input));
    }
}