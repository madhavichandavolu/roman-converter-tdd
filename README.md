# Roman Converter TDD

## Goal
- To convert the given Roman string to number
- Make sure that the source should be 
    - Readable
    - Extensible

## Task
- Make the test pass one by one with no extra OR dead code in source
    - write the bare minimum code to make the test pass
    - Do not modify the test file until all the tests pass

**P.S:**  Feel free to add more tests once all the tests are `Green`